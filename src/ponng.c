
#include <nng/ponng.h>

#define used(x) do {if (x != x) x = x;} while(0);

// req/rep
NNG_DECL
int nng_listen_rep0(int* sock, char* url) {
    used(sock);
    used(url);
    int r = nng_rep0_open((nng_socket*)sock);
    r = nng_listen(*(nng_socket*)sock, url, NULL, 0);
    return r;
}
NNG_DECL
int nng_rep0_do(int sock, int (*h)(int sock, char* buf, int sz, char**repbuf, int* repsz)) {
    used(sock);
    used(h);
    char*rdbuf = NULL;
    int rdsz = 0;

    int r = nng_recv(*((nng_socket*)&sock), rdbuf, (size_t*)(&rdsz), 0);

    char*wrbuf = NULL;
    int wrsz = 0;
    r = h(sock, rdbuf, rdsz, &wrbuf, &wrsz);
    r = nng_send(*((nng_socket*)&sock), wrbuf, wrsz, 0);
    return r;
}
NNG_DECL
int nng_dial_req0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_req0_do(int sock, char* sdbuf, int sdsz, char** rvbuf, int* rvsz) {
    used(sock);
    used(sdbuf);
    used(sdsz);
    used(rvbuf);
    used(rvsz);
    return 0;
}

// pub/sub
NNG_DECL
int nng_listen_pub0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_dial_sub0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_sub0_do(int sock, int(*h)(int sock, char* buf, int sz)) {
    used(sock);
    used(h);
    return 0;
}

// bus
NNG_DECL
int nng_listen_bus0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_bus0_do(int sock, int(*h)(int sock, char* buf, int sz)) {
    used(sock);
    used(h);
    return 0;
}

// serveyor
NNG_DECL
int nng_listen_serveyor0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_serveror0_do(int sock, int(*h)(int sock, char* buf, int sz)) {
    used(sock);
    used(h);
    return 0;
}
NNG_DECL
int nng_dial_respondent0(int* sock, char* url) {
    used(sock);
    used(url);
    return 0;
}
NNG_DECL
int nng_respondent0_do(int sock, int(*h)(int sock, char* buf, int sz)) {
    used(sock);
    used(h);
    return 0;
}


