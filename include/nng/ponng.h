#ifndef PONNG_H
#define PONNG_H

#include <nng/nng.h>

#include <nng/protocol/reqrep0/req.h>
#include <nng/protocol/reqrep0/rep.h>

#include <nng/protocol/pubsub0/pub.h>
#include <nng/protocol/pubsub0/sub.h>

// req/rep
int nng_listen_rep0(int* sock, char* url);
int nng_rep0_do(int sock, int (*h)(int sock, char* buf, int sz, char**repbuf, int* repsz));
int nng_dial_req0(int* sock, char* url);
int nng_req0_do(int sock, char* sdbuf, int sdsz, char** rvbuf, int* rvsz);

// pub/sub
int nng_listen_pub0(int* sock, char* url);
int nng_dial_sub0(int* sock, char* url);
int nng_sub0_do(int sock, int(*h)(int sock, char* buf, int sz));

// bus
int nng_listen_bus0(int* sock, char* url);
int nng_bus0_do(int sock, int(*h)(int sock, char* buf, int sz));

// serveyor
int nng_listen_serveyor0(int* sock, char* url);
int nng_serveror0_do(int sock, int(*h)(int sock, char* buf, int sz));
int nng_dial_respondent0(int* sock, char* url);
int nng_respondent0_do(int sock, int(*h)(int sock, char* buf, int sz));

// TODO data format

#endif

